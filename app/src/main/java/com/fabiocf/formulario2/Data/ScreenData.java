package com.fabiocf.formulario2.Data;

import java.io.Serializable;
import java.util.ArrayList;

public class ScreenData implements Serializable{

    private long tempoInicial;
    private long tempoFinal;
    private int clickCount;
    private ArrayList<ClickMoment> click;

    public ScreenData(){
        clickCount = 0;
        click = new ArrayList<>();
    }

    public void setTempoInicial(long tempoInicial){
        this.tempoInicial = tempoInicial;
    }

    public void setTempoFinal(long tempoFinal){
        this.tempoFinal = tempoFinal;
    }

    public void setClick(ClickMoment click) {
        this.click.add(click);
        clickCount++;
    }

    public long getTempoInicial(){
        return tempoInicial;
    }

    public long getTempoFinal(){
        return tempoFinal;
    }

    public ClickMoment getClick(int index){
        return click.get(index);
    }

    public int getClickCount(){
        return clickCount;
    }
}
