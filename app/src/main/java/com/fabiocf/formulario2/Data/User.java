package com.fabiocf.formulario2.Data;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

    private static final int NUMERO_DE_TELAS = 15;

    private int tamanhoLetra;
    private String nome;
    private String opPress;
    private String opMenu;
    private String opZoom1;
    private String opZoom2;

    private int telaWidth;
    private int telaHeight;

    private ArrayList<ScreenData> tela;

    public User(int telaWidth, int telaHeight){
        nome = "";
        opPress = "";
        opMenu = "";
        opZoom1 = "";
        opZoom2 = "";
        tamanhoLetra = 0;
        this.telaWidth = telaWidth;
        this.telaHeight = telaHeight;
        tela = new ArrayList<>();
        for(int i = 0; i < NUMERO_DE_TELAS; i++)
            tela.add(new ScreenData());
    }

    public int getNumeroDeTelas(){
        return NUMERO_DE_TELAS;
    }

    public int getTamanhoLetra (){
        return tamanhoLetra;
    }

    public void setTamanhoLetra (int tamanhoLetra){
        this.tamanhoLetra = tamanhoLetra;
    }

    public int getTelaWidth (){
        return telaWidth;
    }

    public void setTelaWidth (int telaWidth){
        this.telaWidth = telaWidth;
    }

    public int getTelaHeight (){
        return telaHeight;
    }

    public void setTelaHeight (int telaHeight){
        this.telaHeight = telaHeight;
    }

    public String getNome(){
        return nome;
    }

    public void setNome (String nome){
        this.nome = nome;
    }

    public String getOpPress(){
        return opPress;
    }

    public void setOpPress(String opPress){
        this.opPress = opPress;
    }

    public String getOpMenu(){
        return opMenu;
    }

    public void setOpMenu(String opMenu){
        this.opMenu = opMenu;
    }

    public String getOpZoom1(){
        return opZoom1;
    }

    public void setOpZoom1 (String opZoom1){
        this.opZoom1 = opZoom1;
    }

    public String getOpZoom2(){
        return opZoom2;
    }

    public void setOpZoom2 (String opZoom2){
        this.opZoom2 = opZoom2;
    }

    public ScreenData getTela(int index){
        return tela.get(index);
    }

    public void setTela (ScreenData tela){
        this.tela.add(tela);
    }
}
