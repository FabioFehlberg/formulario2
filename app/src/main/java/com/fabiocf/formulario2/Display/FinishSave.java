package com.fabiocf.formulario2.Display;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;
import com.fabiocf.formulario2.Util.CSVUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class FinishSave extends AppCompatActivity {

    User usuario;
    boolean CSV;
    boolean permission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_save);
        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        permission = Environment.getExternalStorageDirectory().canWrite();
        CSV = false;
    }

    protected void askWritePermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1234);
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1234);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1234: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    permission = true;
                    Toast.makeText(this,"Permissão concedida.", Toast.LENGTH_SHORT).show();
                    generateCSV();

                } else {

                    Toast.makeText(this,"Permissão para escrever na memoria nao concedida.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public void generateCSV(){

        File storageDir = new File(Environment.getExternalStorageDirectory(), "Formulario2");
        storageDir.mkdir();
        String fileName = usuario.getNome() + usuario.getTela(0).getTempoInicial() + ".csv";
        File arquivo = new File(storageDir, fileName);
        try {

            if(!arquivo.exists())
                arquivo.createNewFile();
            FileWriter writer = new FileWriter(arquivo);

            //User info
            CSVUtils.writeLine(writer, Arrays.asList(usuario.getNome()));
            CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTamanhoLetra())));
            CSVUtils.writeLine(writer, Arrays.asList(usuario.getOpMenu()));
            CSVUtils.writeLine(writer, Arrays.asList(usuario.getOpPress()));
            CSVUtils.writeLine(writer, Arrays.asList(usuario.getOpZoom1()));
            CSVUtils.writeLine(writer, Arrays.asList(usuario.getOpZoom2()));
            CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTelaHeight())));
            CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTelaWidth())));

            //telas
            for(int i = 0; i < usuario.getNumeroDeTelas(); i++) {
                CSVUtils.writeLine(writer, Arrays.asList("tela", String.valueOf(i)));
                CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTela(i).getTempoInicial())));
                CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTela(i).getTempoFinal())));
                CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTela(i).getClickCount())));
                //cliques
                for(int j = 0; j < usuario.getTela(i).getClickCount(); j++){
                    CSVUtils.writeLine(writer, Arrays.asList("evento", String.valueOf(j)));
                    CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTela(i).getClick(j).getX())));
                    CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTela(i).getClick(j).getY())));
                    CSVUtils.writeLine(writer, Arrays.asList(String.valueOf(usuario.getTela(i).getClick(j).getTempo())));
                    CSVUtils.writeLine(writer, Arrays.asList(usuario.getTela(i).getClick(j).getAcao()));
                }
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri pathUri = Uri.fromFile(arquivo);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {"fabio.chaves.f@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        // the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, pathUri);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "form2-" + usuario.getNome());
        startActivity(Intent.createChooser(emailIntent , "Send email..."));
        CSV = true;
    }

    public void generate(View v){
        if(!permission)
            askWritePermission();
        else if(!CSV)
            generateCSV();
        else
            Toast.makeText(this,"O arquivo já foi gerado.",Toast.LENGTH_SHORT).show();
    }

    public void next(View v){
        if(CSV){
            Intent it = new Intent(this, WelcomeActivity.class);
            startActivity(it);
        }
        else
            Toast.makeText(this,"Favor gerar o arquivo antes de recomeçar.",Toast.LENGTH_SHORT).show();
    }
}
