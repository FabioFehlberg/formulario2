package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class HomeObjects extends AppCompatActivity {

    private int TELA_ATUAL = 3;

    User usuario;

    ImageView tv;
    boolean tvB;

    ImageView forno;
    boolean fornoB;

    ImageView foguete;
    boolean fogueteB;

    ImageView telefone;
    boolean telefoneB;

    ImageView trem;
    boolean tremB;

    boolean acao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_objects);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        forno = (ImageView) findViewById(R.id.forno);
        tv = (ImageView) findViewById(R.id.tv);
        foguete = (ImageView) findViewById(R.id.foguete);
        telefone = (ImageView) findViewById(R.id.telefone);
        trem = (ImageView) findViewById(R.id.trem);
        fornoB = false;
        tvB = false;
        fogueteB = false;
        telefoneB = false;
        tremB = false;

        acao = false;

        configuraTouch();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void configuraTouch() {
        forno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!fornoB) {
                    forno.setBackgroundColor(Color.GREEN);
                    fornoB = true;
                }
                else {
                    forno.setBackgroundColor(Color.TRANSPARENT);
                    fornoB = false;
                }
                checkAcao();
            }
        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!tvB) {
                    tv.setBackgroundColor(Color.GREEN);
                    tvB = true;
                }
                else {
                    tv.setBackgroundColor(Color.TRANSPARENT);
                    tvB = false;
                }
                checkAcao();
            }
        });
        telefone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!telefoneB) {
                    telefone.setBackgroundColor(Color.GREEN);
                    telefoneB = true;
                }
                else {
                    telefone.setBackgroundColor(Color.TRANSPARENT);
                    telefoneB = false;
                }
                checkAcao();
            }
        });
        trem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!tremB) {
                    trem.setBackgroundColor(Color.RED);
                    tremB = true;
                }
                else {
                    trem.setBackgroundColor(Color.TRANSPARENT);
                    tremB = false;
                }
                checkAcao();
            }
        });
        foguete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!fogueteB) {
                    foguete.setBackgroundColor(Color.RED);
                    fogueteB = true;
                }
                else {
                    foguete.setBackgroundColor(Color.TRANSPARENT);
                    fogueteB = false;
                }
                checkAcao();
            }
        });
    }

    protected void checkAcao(){
        if(fornoB && telefoneB && tvB && !fogueteB && !tremB)
            ativaBotaoProximo(true);
        else
            ativaBotaoProximo(false);
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            acao = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
            acao = false;
        }
    }

    public void proximo(View v){

        if(acao){
            Intent it = new Intent(this, Matching.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        }
        else
            Toast.makeText(this, "Selecione as opções.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
