package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Matching extends AppCompatActivity {

    private int TELA_ATUAL = 4;

    User usuario;

    ImageView livro;
    boolean livroB;

    ImageView balde;
    boolean baldeB;

    ImageView lesma1;
    boolean lesmaB1;
    ImageView lesma2;
    boolean lesmaB2;

    ImageView bolo1;
    boolean boloB1;
    ImageView bolo2;
    boolean boloB2;

    ImageView bolsa;
    boolean bolsaB;

    boolean acao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_matching);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        balde = (ImageView) findViewById(R.id.balde);
        livro = (ImageView) findViewById(R.id.livro);
        lesma1 = (ImageView) findViewById(R.id.lesma1);
        lesma2 = (ImageView) findViewById(R.id.lesma2);
        bolo1 = (ImageView) findViewById(R.id.bolo1);
        bolo2 = (ImageView) findViewById(R.id.bolo2);
        bolsa = (ImageView) findViewById(R.id.bolsa);
        baldeB = false;
        livroB = false;
        lesmaB1 = false;
        lesmaB2 = false;
        boloB1 = false;
        bolsaB = false;

        acao = false;

        configuraTouch();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void configuraTouch() {
        balde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!baldeB) {
                    balde.setBackgroundColor(Color.RED);
                    baldeB = true;
                }
                else {
                    balde.setBackgroundColor(Color.TRANSPARENT);
                    baldeB = false;
                }
                checkAcao();

            }
        });
        livro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!livroB) {
                    livro.setBackgroundColor(Color.RED);
                    livroB = true;
                }
                else {
                    livro.setBackgroundColor(Color.TRANSPARENT);
                    livroB = false;
                }
                checkAcao();

            }
        });
        bolo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!boloB1) {
                    bolo1.setBackgroundColor(Color.GREEN);
                    boloB1 = true;
                }
                else {
                    bolo1.setBackgroundColor(Color.TRANSPARENT);
                    boloB1 = false;
                }
                checkAcao();

            }
        });
        bolo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!boloB2) {
                    bolo2.setBackgroundColor(Color.GREEN);
                    boloB2 = true;
                }
                else {
                    bolo2.setBackgroundColor(Color.TRANSPARENT);
                    boloB2 = false;
                }
                checkAcao();

            }
        });
        lesma2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!lesmaB2) {
                    lesma2.setBackgroundColor(Color.GREEN);
                    lesmaB2 = true;
                }
                else {
                    lesma2.setBackgroundColor(Color.TRANSPARENT);
                    lesmaB2 = false;
                }
                checkAcao();

            }
        });
        bolsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bolsaB) {
                    bolsa.setBackgroundColor(Color.RED);
                    bolsaB = true;
                }
                else {
                    bolsa.setBackgroundColor(Color.TRANSPARENT);
                    bolsaB = false;
                }
                checkAcao();

            }
        });
        lesma1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!lesmaB1) {
                    lesma1.setBackgroundColor(Color.GREEN);
                    lesmaB1 = true;
                }
                else {
                    lesma1.setBackgroundColor(Color.TRANSPARENT);
                    lesmaB1 = false;
                }
                checkAcao();
            }
        });
    }

    protected void checkAcao(){
        if(lesmaB1 && lesmaB2 && boloB1 && boloB2 && !bolsaB && !livroB && !baldeB)
            ativaBotaoProximo(true);
        else
            ativaBotaoProximo(false);
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            acao = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
            acao = false;
        }
    }

    public void proximo(View v){

        if(acao){
            Intent it = new Intent(this, Zoom1.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        }
        else
            Toast.makeText(this, "Selecione as opções.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
