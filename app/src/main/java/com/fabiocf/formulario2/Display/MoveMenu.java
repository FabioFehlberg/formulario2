package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class MoveMenu extends AppCompatActivity {

    private int TELA_ATUAL = 8;

    User usuario;

    ImageView lixeira;
    ImageView config;
    ImageView info;
    ImageView pasta;

    boolean soltouDeleta;
    boolean deletou;

    public float iconeAndroid_height;
    public float iconeAndroid_width;
    public Rect rectPasta;
    public Rect rectLixeira;
    public Rect rectInfo;
    public Rect rectConfig;

    private float iconeX;
    private float iconeY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_move_menu);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        lixeira = (ImageView) findViewById(R.id.lixeira);
        info = (ImageView) findViewById(R.id.info);
        config = (ImageView) findViewById(R.id.config);
        pasta = (ImageView) findViewById(R.id.pasta);

        soltouDeleta = false;
        deletou = false;


    }

    @Override
    protected void onResume() {
        super.onResume();
        pasta.post(new Runnable() {
            @Override
            public void run() {
                iconeAndroid_height = pasta.getHeight();
                iconeAndroid_width = pasta.getWidth();
                iconeX = pasta.getX();
                iconeY = pasta.getY();
                configuraTouch();
            }
        });
    }

    //https://www.youtube.com/watch?v=plnLs6aST1M - acessado 25/05
    public void helpMenu(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(MoveMenu.this);
        View view = getLayoutInflater().inflate(R.layout.move_menu_dialog, null);

        Button fecharBt = (Button) view.findViewById(R.id.fecharDialog);
        TextView ajuda = (TextView) view.findViewById(R.id.PMtexto);
        ajuda.setTextSize(usuario.getTamanhoLetra());

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        fecharBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
    }

    //https://www.javatpoint.com/android-popup-menu-example - 11-10
    public void callPopMenu(){
        PopupMenu popup = new PopupMenu(MoveMenu.this, pasta);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                usuario.setOpMenu(item.getTitle().toString());
                if(item.getItemId() == R.id.excluir){
                    ativaBotaoProximo(true);
                    pasta.setClickable(false);
                    pasta.setVisibility(View.INVISIBLE);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public void configuraTouch() {
        pasta.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent m) {
                float x = m.getRawX();
                float y = m.getRawY() - 100;

                switch (m.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(m.getX(),m.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                    case MotionEvent.ACTION_POINTER_DOWN:
                        return true;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        if(soltouDeleta && usuario.getOpMenu().equals("Excluir")){
                            pasta.setVisibility(View.INVISIBLE);
                            pasta.setClickable(false);
                            ativaBotaoProximo(true);
                        }
                        else {
                            config.setBackgroundColor(Color.TRANSPARENT);
                            info.setBackgroundColor(Color.TRANSPARENT);
                            pasta.setX(iconeX);
                            pasta.setY(iconeY);
                            callPopMenu();
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(m.getX(),m.getY(),System.currentTimeMillis(),"ACTION_MOVE"));
                        //tratamento para arrastar imagem
                        pasta.setX(((int) x) - (iconeAndroid_width / 2));
                        pasta.setY(((int) y) - (iconeAndroid_height / 2));

                        //caso a imagem colida com a outra - https://developer.android.com/reference/android/graphics/Rect.html#intersects%28android.graphics.Rect,%20android.graphics.Rect%29
                        rectPasta = new Rect();
                        rectConfig = new Rect();
                        rectInfo = new Rect();
                        rectLixeira = new Rect();
                        pasta.getHitRect(rectPasta);
                        config.getHitRect(rectConfig);
                        info.getHitRect(rectInfo);
                        lixeira.getHitRect(rectLixeira);
                        if (Rect.intersects(rectPasta, rectLixeira)) {
                            lixeira.setBackgroundColor(Color.GREEN);
                            usuario.setOpMenu("Excluir");
                            soltouDeleta = true;
                        } else if(Rect.intersects(rectPasta, rectConfig)){
                            config.setBackgroundColor(Color.GREEN);
                            usuario.setOpMenu("Configuracoes");
                            soltouDeleta = true;
                        } else if (Rect.intersects(rectPasta, rectInfo)){
                            info.setBackgroundColor(Color.GREEN);
                            usuario.setOpMenu("Informacoes");
                            soltouDeleta = true;
                        }
                        else{
                            lixeira.setBackgroundColor(Color.TRANSPARENT);
                            config.setBackgroundColor(Color.TRANSPARENT);
                            info.setBackgroundColor(Color.TRANSPARENT);
                            soltouDeleta = false;
                        }
                        return true;
                }
                return false;
            }
        });
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            deletou = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
            deletou = false;
        }
    }

    public void proximo(View v){

        if(deletou){
            Intent it = new Intent(this, Rotation1.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
            //finish();
        }
        else
            Toast.makeText(this, "Tente deletar antes de prosseguir.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
