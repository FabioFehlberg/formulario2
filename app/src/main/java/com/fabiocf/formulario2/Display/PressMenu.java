package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class PressMenu extends AppCompatActivity {

    private int TELA_ATUAL = 7;

    User usuario;

    ImageView pasta;

    boolean soltouDeleta;
    boolean deletou;

    //https://www.javatpoint.com/android-popup-menu-example - 11-10
    public void callPopMenu(){
        PopupMenu popup = new PopupMenu(PressMenu.this, pasta);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                usuario.setOpPress(item.getTitle().toString());
                if(item.getItemId() == R.id.excluir){
                    ativaBotaoProximo(true);
                    pasta.setVisibility(View.INVISIBLE);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    //https://www.youtube.com/watch?v=plnLs6aST1M - acessado 25/05
    public void helpMenu(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(PressMenu.this);
        View view = getLayoutInflater().inflate(R.layout.press_menu_dialog, null);

        Button fecharBt = (Button) view.findViewById(R.id.fecharDialog);
        TextView ajuda = (TextView) view.findViewById(R.id.PMtexto);
        ajuda.setTextSize(usuario.getTamanhoLetra());

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        fecharBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_press_menu);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        pasta = (ImageView) findViewById(R.id.pasta);
        pasta.setOnLongClickListener(new View.OnLongClickListener() {
                                         public boolean onLongClick(View v) {
                                             callPopMenu();
                                             return true;
                                         }
                                     });
        soltouDeleta = false;
        deletou = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            deletou = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
            deletou = false;
        }
    }

    public void proximo(View v){

        if(deletou){
            Intent it = new Intent(this, MoveMenu.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        }
        else
            Toast.makeText(this, "Tente deletar antes de prosseguir.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
