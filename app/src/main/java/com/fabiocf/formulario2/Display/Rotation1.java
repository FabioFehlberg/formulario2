package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Rotation1 extends AppCompatActivity {

    private int TELA_ATUAL = 9;

    User usuario;

    ImageView carro;

    float angle;

    boolean firstClick;

    boolean rotacionou;

    private int rotCont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rotation1);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        carro = (ImageView) findViewById(R.id.carro);
        carro.setImageResource(R.drawable.car);
        carro.setScaleType(ImageView.ScaleType.CENTER);

        angle = 180;
        rotCont = 0;

        rotacionou = false;
    }

    @Override
    public void onResume(){
        super.onResume();
        carro.setRotation(angle);
    }

    //https://www.youtube.com/watch?v=plnLs6aST1M - acessado 25/05
    public void helpMenu(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(Rotation1.this);
        View view = getLayoutInflater().inflate(R.layout.rotation1_menu_dialog, null);

        Button fecharBt = (Button) view.findViewById(R.id.fecharDialog);
        TextView ajuda = (TextView) view.findViewById(R.id.PMtexto);
        TextView ajuda1 = (TextView) view.findViewById(R.id.textView2);
        TextView ajuda2 = (TextView) view.findViewById(R.id.textView3);
        ajuda.setTextSize(usuario.getTamanhoLetra());
        ajuda1.setTextSize(usuario.getTamanhoLetra());
        ajuda2.setTextSize(usuario.getTamanhoLetra());

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        fecharBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto é Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i), ev.getY(i), System.currentTimeMillis(), "ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                firstClick = true;
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    public void rotateLeftClick(View v){
        angle-=30;
        carro.setRotation(angle);
        rotCont++;
        if(rotCont%6 == 0)
            if (rotCont%12 != 0)
                ativaBotaoProximo(true);
        else
            ativaBotaoProximo(false);
    }

    public void rotateRightClick(View v){
        angle+=30;
        carro.setRotation(angle);
        rotCont--;
        if(rotCont%6 == 0) {
            if (rotCont%12 != 0)
                ativaBotaoProximo(true);
        }
        else
            ativaBotaoProximo(false);
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            rotacionou = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
            rotacionou = false;
        }
    }

    public void proximo(View v) {
        if (rotacionou) {
            Intent it = new Intent(this, Rotation2.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        } else
            Toast.makeText(this, "Tente rotacionar o carro antes de prosseguir.", Toast.LENGTH_SHORT).show();
    }

}
