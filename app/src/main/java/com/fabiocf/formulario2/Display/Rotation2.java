package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Rotation2 extends AppCompatActivity {

    private int TELA_ATUAL = 10;

    User usuario;

    ImageView cavalo;
    float angle;
    float angle1;
    float lastAngle;
    float centerX;
    float centerY;
    float fX;
    float fY;
    float newfX;
    float newfY;

    boolean rotacionou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rotation2);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        cavalo = (ImageView) findViewById(R.id.cavalo);

        lastAngle = 180;

        rotacionou = false;
    }

    @Override
    public void onResume(){
        super.onResume();
        cavalo.setRotation(lastAngle);
    }

    //https://www.youtube.com/watch?v=plnLs6aST1M - acessado 25/05
    public void helpMenu(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(Rotation2.this);
        View view = getLayoutInflater().inflate(R.layout.rotation2_menu_dialog, null);

        Button fecharBt = (Button) view.findViewById(R.id.fecharDialog);
        TextView ajuda = (TextView) view.findViewById(R.id.PMtexto);
        ajuda.setTextSize(usuario.getTamanhoLetra());

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        fecharBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch  -  o resto � Android Developer
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                fX = ev.getX();
                fY = ev.getY();
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                newfX = ev.getX();
                newfY = ev.getY();
                Rect rectCavalo = new Rect();
                cavalo.getGlobalVisibleRect(rectCavalo);
                centerX = rectCavalo.exactCenterX();
                centerY = rectCavalo.exactCenterY();
                angle = angleBetweenLines (centerX, centerY, fX, fY, centerX, centerY, newfX, newfY);
                cavalo.setRotation(-(angle+lastAngle));
                if((angle+lastAngle)%360 < 15 && (angle+lastAngle)%360 > -15)
                    ativaBotaoProximo(true);
                else
                    ativaBotaoProximo(false);
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                lastAngle += angle;
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    private float angleBetweenLines (float fX, float fY, float sX, float sY, float nfX, float nfY, float nsX, float nsY) {
        float angle1 = (float) Math.atan2( (fY - sY), (fX - sX) );
        float angle2 = (float) Math.atan2( (nfY - nsY), (nfX - nsX) );

        float angle = ((float)Math.toDegrees(angle1 - angle2)) % 360;
        if (angle < -180.f) angle += 360.0f;
        if (angle > 180.f) angle -= 360.0f;
        return angle;
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            rotacionou = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
            rotacionou = false;
        }
    }


    public void proximo(View v) {
        if (rotacionou) {
            Intent it = new Intent(this, FinishSave.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        } else
            Toast.makeText(this, "Tente rotacionar o cavalo antes de prosseguir.", Toast.LENGTH_SHORT).show();
    }

}