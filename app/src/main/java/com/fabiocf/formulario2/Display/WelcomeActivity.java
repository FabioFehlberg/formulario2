package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class WelcomeActivity extends AppCompatActivity {

    private int SCREEN = 0;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        user = new User(displayMetrics.widthPixels,displayMetrics.heightPixels);

        user.getTela(SCREEN).setTempoInicial(System.currentTimeMillis());
    }

    public void next(View v){
        Intent it = new Intent(this, TextSize.class);
        user.getTela(SCREEN).setTempoFinal(System.currentTimeMillis());
        it.putExtra("usuario", user);
        startActivity(it);
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                user.getTela(SCREEN).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    user.getTela(SCREEN).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                user.getTela(SCREEN).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }
}
