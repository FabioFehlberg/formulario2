package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Zoom1 extends FragmentActivity {

    private int TELA_ATUAL = 5;

    User usuario;

    ImageView cachorro;
    ViewGroup.LayoutParams params;

    ImageView zoomIn;
    ImageView zoomOut;

    RadioButton r1;
    RadioButton r2;
    RadioButton r3;
    RadioButton r4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_zoom1);

        cachorro = (ImageView) findViewById(R.id.cachorro);
        zoomIn = (ImageView) findViewById(R.id.zoomin);
        zoomOut = (ImageView) findViewById(R.id.zoomout);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        r1 = (RadioButton) findViewById(R.id.animal1);
        r1.setTextSize(usuario.getTamanhoLetra());
        r2 = (RadioButton) findViewById(R.id.animal2);
        r2.setTextSize(usuario.getTamanhoLetra());
        r3 = (RadioButton) findViewById(R.id.animal3);
        r3.setTextSize(usuario.getTamanhoLetra());
        r4 = (RadioButton) findViewById(R.id.animal4);
        r4.setTextSize(usuario.getTamanhoLetra());

    }

    @Override
    protected void onResume(){
        super.onResume();
        zoomIn.setVisibility(View.VISIBLE);
        zoomOut.setVisibility(View.VISIBLE);
    }

    public void animalClick(View v){
        ativaBotaoProximo(true);
        switch (Integer.parseInt(v.getTag().toString())){
            case 1:
                usuario.setOpZoom1("gato");
                r2.setChecked(false);
                r3.setChecked(false);
                r4.setChecked(false);
                break;
            case 2:
                usuario.setOpZoom1("cachorro");
                r1.setChecked(false);
                r3.setChecked(false);
                r4.setChecked(false);
                break;
            case 3:
                r2.setChecked(false);
                r1.setChecked(false);
                r4.setChecked(false);
                usuario.setOpZoom1("rato");
                break;
            case 4:
                r2.setChecked(false);
                r3.setChecked(false);
                r1.setChecked(false);
                usuario.setOpZoom1("leao");
                break;
        }
    }

    public void zoomInClick(View v){
        params = cachorro.getLayoutParams();
        if(params.height < 500 && params.width < 500) {
            params.height = params.height + 25;
            params.width = params.width + 25;
            cachorro.setLayoutParams(params);
        }
    }

    public void zoomOutClick(View v){
        params = cachorro.getLayoutParams();
        if(params.height > 40 && params.width > 40) {
            params.height = params.height - 25;
            params.width = params.width - 25;
            cachorro.setLayoutParams(params);
        }
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
            //acao = true;
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
        }
    }

    public void proximo(View v){
        if(!usuario.getOpZoom1().equals("")){
            Intent it = new Intent(this, Zoom2.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        }
        else
            Toast.makeText(this, "Selecione uma opção para prosseguir.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}
