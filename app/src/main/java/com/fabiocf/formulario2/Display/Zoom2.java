package com.fabiocf.formulario2.Display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fabiocf.formulario2.Data.ClickMoment;
import com.fabiocf.formulario2.Data.User;
import com.fabiocf.formulario2.R;

import static android.view.MotionEvent.ACTION_CANCEL;

public class Zoom2 extends AppCompatActivity {

    private int TELA_ATUAL = 6;

    User usuario;

    ImageView cachorro;
    ViewGroup.LayoutParams params;
    int cachorroH;
    int cachorroW;
    SeekBar seekZoom;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_zoom2);

        usuario = (User) getIntent().getExtras().getSerializable("usuario");
        usuario.getTela(TELA_ATUAL).setTempoInicial(System.currentTimeMillis());

        TextView textView = (TextView) findViewById(R.id.titulo);
        textView.setTextSize(usuario.getTamanhoLetra());

        cachorro = (ImageView) findViewById(R.id.lion);
        seekZoom = (SeekBar) findViewById(R.id.seekZoom);



        seekZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {

                params.width = (progresValue+1)*7 + cachorroW;
                params.height = (progresValue+1)*7 + cachorroH;
                cachorro.setLayoutParams(params);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        RadioButton r = (RadioButton) findViewById(R.id.animal1);
        r.setTextSize(usuario.getTamanhoLetra());
        r = (RadioButton) findViewById(R.id.animal2);
        r.setTextSize(usuario.getTamanhoLetra());
        r = (RadioButton) findViewById(R.id.animal3);
        r.setTextSize(usuario.getTamanhoLetra());
        r = (RadioButton) findViewById(R.id.animal4);
        r.setTextSize(usuario.getTamanhoLetra());
    }

    @Override
    protected void onResume(){
        super.onResume();
        params = cachorro.getLayoutParams();
        cachorroH = params.height/5;
        cachorroW = params.width/5;
        params.height = cachorroH;
        params.width = cachorroW;
        cachorro.setLayoutParams(params);
    }

    public void animalClick(View v){
        ativaBotaoProximo(true);
        switch (Integer.parseInt(v.getTag().toString())){
            case 1:
                usuario.setOpZoom2("gato");
                break;
            case 2:
                usuario.setOpZoom2("cachorro");
                break;
            case 3:
                usuario.setOpZoom2("rato");
                break;
            case 4:
                usuario.setOpZoom2("leao");
                break;
        }
    }

    public void ativaBotaoProximo(boolean ativa){
        Button proximo = (Button) findViewById(R.id.button);
        if(ativa) {
            proximo.setBackgroundColor(Color.RED);
        }
        else {
            proximo.setBackgroundColor(Color.LTGRAY);
        }
    }

    public void proximo(View v){
        if(!usuario.getOpZoom2().equals("")){
            Intent it = new Intent(this, PressMenu.class);
            usuario.getTela(TELA_ATUAL).setTempoFinal(System.currentTimeMillis());
            it.putExtra("usuario", usuario);
            startActivity(it);
        }
        else
            Toast.makeText(this, "Selecione uma opção para prosseguir.", Toast.LENGTH_SHORT).show();
    }

    //http://www.vogella.com/tutorials/AndroidTouch/article.html - MultiTouch
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){

        // get masked (not specific to a pointer) action
        int maskedAction = ev.getActionMasked();

        switch (maskedAction) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_DOWN"));
                break;
            }
            case MotionEvent.ACTION_MOVE: { // a pointer was moved
                for (int size = ev.getPointerCount(), i = 0; i < size; i++) {
                    usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(i),ev.getY(i),System.currentTimeMillis(),"ACTION_MOVE"));
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
            case ACTION_CANCEL: {
                usuario.getTela(TELA_ATUAL).setClick(new ClickMoment(ev.getX(),ev.getY(),System.currentTimeMillis(),"ACTION_CANCEL"));
                break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

}
